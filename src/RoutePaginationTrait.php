<?php
namespace Lmpsolution\RoutePagination;
use Illuminate\Database\Eloquent\Builder;
trait RoutePaginationTrait 
{
    /**
     * @param $query
     * @return Builder
     * @throws \RuntimeException
     */
    public function newEloquentBuilder($query)
    {
        static $class;
        if (! $class) {
            $class = \Lmpsolution\RoutePagination\RoutePaginationBuilder::class;
        }
        $builder = new $class($query);
        if ($builder instanceof Builder === false) {
            throw new \RuntimeException("Builder class must extend 'Illuminate\\Database\\Eloquent\\Builder' class", 500);
        }
        return $builder;
    }
} 
