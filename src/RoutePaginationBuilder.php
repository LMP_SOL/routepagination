<?php
namespace Lmpsolution\RoutePagination;

use \Illuminate\Pagination\Paginator;
use \Illuminate\Database\Eloquent\Builder;
class RoutePaginationBuilder extends Builder
{
    /**
     * Rewrite Route Paginate the given query.
     *
     * @param  int  $perPage
     * @param  array  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \lmpsolution\RoutePagination\RoutePaginationAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function routePaginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null, $routeName=null)
    {
        $page = $page ?: Paginator::resolveCurrentPage($pageName);
        $req = Request();
        if(isset($req->$pageName) && $page == 1 && is_numeric($req->$pageName)){
            $page = intval($req->$pageName);
        }

        if($page < 1) $page = 1;


        $perPage = $perPage ?: $this->model->getPerPage();

        $results = ($total = $this->toBase()->getCountForPagination())
                                    ? $this->forPage($page, $perPage)->get($columns)
                                    : $this->model->newCollection();

        $routePaginate = new RoutePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);
        if($routeName != null){
            $routePaginate->setRouteName($routeName);
        }
        return $routePaginate;
    }
}