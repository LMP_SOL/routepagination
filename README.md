# README #
composer require lmpsolution/routepagination

# how to init #
1 open Model
2 outside class 
	use Lmpsolution\RoutePagination\RoutePaginationTrait;
3 inslide class
	use RoutePaginationTrait;

# how to use #
call function 
	->routePagination($route_name, $per_page);
